---
title: "Creating a JHipster Playwright Blueprint - Part 1"
date: 2021-03-10
type: "post"
draft: false
featured: "jhipster-playwright-part-1.png"
featuredpath: "featured"
featuredalt: "JHipsters and Playwright Logo"
tags: ["jhipster", "playwright", "jhipster-blueprint", "customization", "java", "angular", "vue", "jhipster-playwright-series"]
author: "Frederik Hahne"
---

:source-highlighter: rouge
:rouge-style: monokai
:icons: font

https://www.jhipster.tech/running-tests/[JHipster offers good support for doing end-2-end tests^]. 
Recently Cypress has been added as the default option and Protractor has been deprecated.

While maintaining protractor tests for three frontend stacks (angular, react and vue) was not easy, 
we are very happy with what Cypress offers. 
The tests are much more stable and the developer experience is great.
Nevertheless Cypress has some drawbacks as it focuses on doing one thing well.

When using JHipster with oAuth https://github.com/jhipster/generator-jhipster/pull/13611[an interactive login is not possible with pure Cypress^].
Right now we leverage https://github.com/puppeteer/puppeteer[Puppeteer^] to automate the browser and do the interactive login.
While this works it feels like a hack and really slows down the overall time for running the tests.

Recently https://github.com/jhipster/generator-jhipster/issues/13755[Playwright has come into play^] as an alternative cross browser automation framework.
https://playwright.dev/[Playwright^] supports different languages, can automate real browsers and supports multiple origins per test.
Furthermore Playwright seems to be faster and more lightweight. 
But Playwright lacks the e.g. the support for time traveling which makes debugging tests a breeze. 

This blog series will try to integrate Playwright into JHipster instead of Cypress.
Over the course of multiple post we will adapt a generated application to use Playwright instead of Cypress,
using both the https://github.com/microsoft/playwright[playwright for node^] and https://github.com/microsoft/playwright-java[playwrigth-java^].
Eventually I show how to develop and release a https://www.jhipster.tech/modules/creating-a-blueprint/[JHipster blueprint^] will generate all required files to use Playwright in JHipster without
any manual steps.

## Further Reading 

* https://playwright.dev/[Offical Playwright Homepage^]
* https://www.cypress.io/[Offical Cypress Homepage^]
* https://medium.com/sparebank1-digital/playwright-vs-cypress-1e127d9157bd[Playwright vs Cypress Comparison^]
* https://medium.com/uk-hydrographic-office/why-we-chose-playwright-over-cypress-db4770cf5204[Why we chose Playwright over Cypress^]
* https://blog.checklyhq.com/cypress-vs-selenium-vs-playwright-vs-puppeteer-speed-comparison/[Cypress vs Selenium vs Playwright vs Puppeteer speed comparison^]
