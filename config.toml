baseURL = "https://atomfrede.gitlab.io"
languageCode = "en-us"
title = "Shiny Adventures of a software developer"
theme = "beautifulhugo"
preserveTaxonomyNames   = true
paginate                = 6
disqusShortname         = ""
googleAnalytics         = ""
pluralizeListTitles     = false
disableLanguages        = [""]
copyright = "CC BY-SA 4.0"

[permalinks]
  posts = "/:year/:month/:title/"

[params]
  mainSections = ["blog", "post"]
  subtitle = "Frederik's Blog"
  rss = true
  logo = "main/fred.jpg" # Expecting square dimensions
  # Loads CSS and JavaScript files. The variable is an array so that you can load
  # multiple/additional files if necessary. The standard theme files can be loaded
  # by adding the value, "default". Default includes the add-on.css and and-on.js.
  # Example: ["default", "/path/to/file"]
  cssFiles              = ["default", "/css/custom.css"]
  jsFiles               = ["default"]
  since = "2013"
  # Sets options for highlight.js
  highlightjs           = false
  highlightjsTheme      = "github"
  highlightjsLang       = ["yaml", "gradle", "groovy", "typescript"]
  # Sets where "View More Posts" links to
  viewMorePostsLink     = "/posts/"
  # Activate Estimated Reading Times, which appear in the post headers
  readingTime           = true
  # Sets which Social Share links appear in posts.
  # Options are twitter, facebook, reddit, linkedin, stumbleupon, pinterest, email
  socialShare           = ["linkedin"]
  hideEmptyStats        = true
  [params.meta]
    # Sets the meta tag description
    description         = "Frederik Hahne's (@atomfrede) blog about software development and stuff. Powered by Hugo."
    # Sets the meta tag author
    # author              = "Frederik Hahne"
    # If you would like to add more comprehensive favicon support passed root
    # directory favicon.ico, utlize this funtion. It is recommened to use
    # https://realfavicongenerator.net to generate your icons as that is the basis
    # for the theme's naming.
    favicon             = false
    svg                 = false
    faviconVersion      = "1"
    msColor             = "#ffffff" # Copy from the https://realfavicongenerator.net
    iOSColor            = "#ffffff" # Copy from the https://realfavicongenerator.net



  # These are optional params related to the sidebar. They are recommended, but not
  # required for proper functionality. HTML is supported within the params.
  [params.intro]
    header                = "Shiny Adventures"
    paragraph             = "of a software developer, <a href='https://twitter.com/jhipster' target='_blank' rel='noopener'>@jhipster</a> board member, <a href='https://twitter.com/jugpaderborn' target='_blank' rel='noopener'>@jugpaderborn</a> organizer and father"
    rssIntro              = true
    socialIntro           = true

    # This appears at the top of the sidebar above params.intro.header.
    # A width of less than 100px is recommended from a design perspective.
    [params.intro.pic]
      src                 = "/main/fred.jpg"
      # Masks image in a certain shape. Supported are circle, triangle, diamond, and hexagon.
      shape               = "hexagon"
      width               = ""
      alt                 = "Shiny Adventures"

  [params.sidebar]
    about               = ""
    # Sets the number of recent posts to show in the sidebar. The default value is 5.
    postAmount          = 2
    # set to show or to hide categories in the sidebar
    categories          = false
    # Sets Categories to sort by number of posts instead of alphabetical
    categoriesByCount   = false

  [params.footer]
    # Sets RSS icons to appear on the sidebar with the social media buttons
    rssFooter           = true
    # Sets Social Media icons to appear on the sidebar
    socialFooter        = true

  # Disqus will take priority over Staticman (github.com/eduardoboucas/staticman)
  # due to its ease of use. Feel free to check out both and decide which you would
  # prefer to use. See Staticman.yml for additional settings.
  [params.staticman]
    # Sets Statiman to be active
    staticman           = false
    # Sets the location for Staticman to connect to
    username            = ""
    repo                = ""
    branch              = ""

    [params.staticman.recaptcha]
      recaptcha         = false
      siteKey           = "" # Site Key
      encryptedKey      = ""

[menu]
  # Sets the menu items in the navigation bar
  # Identifier prepends a Font Awesome icon to the menu item
  [[menu.main]]
    name              = "Home"
    url               = "/"
    identifier        = "fas fa-home"
    weight            = 1

  [[menu.main]]
    name              = "Talks & Publications"
    url               = "/talks/"
    identifier        = "fab fa-slideshare"
    weight            = 2

  [[menu.main]]
    name              = "Blog"
    url               = "/posts/"
    identifier        = "fas fa-blog"
    weight            = 3

  [[menu.main]]
    name              = "About"
    url               = "/about/"
    identifier        = "fas fa-id-card-alt"
    weight            = 6
  [[menu.main]]
      name = "Tags"
      url = "tags"
      weight = 3

[Languages]
  # Each language has its own menu.
  [Languages.en]
    languageCode        = "en"
    LanguageName        = "English"
    weight              = 1

[Author]
  # Set only your "username" for default hosts and full URLs otherwise (e.g., "https://MyGitLab.org/username")
  name = "Frederik Hahne"

  github = "atomfrede"
  gitlab = "atomfrede"
  linkedin = "frederikhahne"
  xing = "Frederik_Hahne"
  stackoverflow = "users/2151471/atomfrede"
  mastodon = "mastodon.social/@atomfrede"



[security]
  enableInlineShortcodes = false
  [security.exec]
    allow = ['^dart-sass-embedded$', '^go$', '^npx$', '^postcss$', '^asciidoctor$']
